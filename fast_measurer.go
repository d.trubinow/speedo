package speedo

import (
	"gopkg.in/ddo/go-fast.v0"
)

//speedTestWrapper wraps "go-fast"-package
type fastWrapper struct {
	client *fast.Fast
}

//Init initiates go-fast client
func (f *fastWrapper) Init() {
	fastCom := fast.New()
	err := fastCom.Init()
	if err != nil {
		panic(err)
	}

	f.client = fastCom
}

//Measure calculates download
//Returns Result("Upload"-speed will always contain zero-value)
//Returns error if DownloadTest failed
func (f *fastWrapper) Measure() (Result, error) {
	if f.client == nil {
		f.Init()
	}

	urls, err := f.client.GetUrls()
	if err != nil {
		return Result{}, err
	}

	kbpsChan := make(chan float64)
	resChan := make(chan float64)

	go func() {
		var res float64
		for kbps := range kbpsChan {
			res = kbps/1000
		}

		resChan <- res
	}()

	err = f.client.Measure(urls, kbpsChan)
	if err != nil {
		return Result{}, err
	}

	var res Result
	res.Download = <- resChan

	return res, nil
}
