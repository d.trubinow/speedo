package speedo

import (
	"github.com/cbergoon/speedtest-go"
)

//speedTestWrapper wraps "speedtest-go"-package
type speedtestWrapper struct {
	targets speedtest.Servers
}

//Init initiates targets(servers)
func (f *speedtestWrapper) Init() {
	user, _ := speedtest.FetchUserInfo()
	serverList, _ := speedtest.FetchServerList(user)
	targets, _ := serverList.FindServer([]int{})

	f.targets = targets
}

//Measure calculates download and upload speed
//Returns Result
//Returns error if DownloadTest or UploadTest failed
func (f *speedtestWrapper) Measure() (Result, error) {
	var res Result

	if f.targets.Len() == 0 {
		f.Init()
	}

	var err error
	for _, s := range f.targets {
		err = s.DownloadTest()
		if err != nil {
			return Result{}, err
		}

		err = s.UploadTest()
		if err != nil {
			return Result{}, err
		}

		res.Download = s.DLSpeed
		res.Upload = s.ULSpeed
	}

	return res, nil
}
