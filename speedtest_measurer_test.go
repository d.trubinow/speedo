package speedo

import (
	"testing"
)

func TestSpeedTestMeasure(t *testing.T) {
	ms, err := New("speedtest")

	if err != nil {
		t.Errorf("error: %v", err)
	}

	res, err := ms.Measure()
	if err != nil {
		t.Errorf("error: %v", err)
	}

	if res.Download <= 0 {
		t.Error("error: wrong Download value")
	}
}

