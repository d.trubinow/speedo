package speedo

import "errors"

// Result struct fields contain download and upload speed in Mb's .
type Result struct {
	Download float64
	Upload float64
}

// Measurer is implemented by any value that has a Measure method
// The Measure method is used to calculate download/upload speed in Mb's
// Measure returns Result type object.
type Measurer interface {
	Measure() (Result, error)
}

//Speedo contains measurer field
type Speedo struct {
	Measurer
}

// New returns a new Speedo with
// "kind"-parameter could be "speedtest" or "fast"
// If "kind" input parameter is not "speedtest" or "fast", err will be returned.
func New(kind string) (*Speedo, error) {
    var measurer Measurer

	switch kind {
	case "fast":
		measurer = &fastWrapper{}
	case "speedtest":
		measurer = &speedtestWrapper{}
	}

	if measurer == nil {
		return nil, errors.New("unsupported parameter")
	}

	return &Speedo{
		measurer,
	}, nil
}

