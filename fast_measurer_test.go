package speedo

import (
	"testing"
)

func TestFastMeasure(t *testing.T) {
	ms, err := New("fast")

	if err != nil {
		t.Errorf("error: %v", err)
	}

	res, err := ms.Measure()
	if err != nil {
		t.Errorf("error: %v", err)
	}

	if res.Download == 0 {
		t.Error("error happened")
	}
}

