package speedo

import (
	"testing"
)

func TestNew1(t *testing.T) {
	_, err := New("fast1")

	if err == nil {
		t.Error("'unsupported parameter'-error should be here")
	}
}

func TestNew2(t *testing.T) {
	_, err := New("fast")

	if err != nil {
		t.Errorf("error: %v", err)
	}
}

func TestNew3(t *testing.T) {
	_, err := New("speedtest")

	if err != nil {
		t.Errorf("error: %v", err)
	}
}

func BenchmarkFastComMeasure(b *testing.B) {
	res, _ := New("fast")
	for i :=0; i < b.N ; i++{
		res.Measure()
	}
}

func BenchmarkSpeedtestMeasure(b *testing.B) {
	res, _ := New("speedtest")
	for i :=0; i < b.N ; i++{
		res.Measure()
	}
}
