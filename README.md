# Speedo wrapper library 
Library to meter upload/download speed using both [Ookla's https://speedtest.net/](https://www.speedtest.net/) and [Netflix's https://fast.com](https://fast.com/).

## API Installation
```
go get github.com/d.trubinov/speedo
```

## API Usage
The code below is sample of usage.
```go
package main 

import (
    "fmt"
    "gitlab.com/d.trubinow/speedo"
)

func main() {
    fmt.Println("Fast.com")
    s, err := speedo.New("fast")
    if err != nil {
        fmt.Println(err)
    }

    res, err := s.Measure()
    if err != nil {
        fmt.Println(err)
    }

    fmt.Println(res)
}
```