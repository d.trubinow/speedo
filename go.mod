module gitlab.com/d.trubinow/speedo

go 1.17

require (
	github.com/cbergoon/speedtest-go v1.1.0
	gopkg.in/ddo/go-fast.v0 v0.0.0-20190807090025-2ab4a3dd618c
)

require (
	github.com/ddo/pick-json v0.0.0-20170207095303-c8760e09e0fe // indirect
	github.com/ddo/rq v0.0.0-20190828174524-b3daa55fcaba // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	gopkg.in/ddo/go-dlog.v2 v2.1.0 // indirect
	gopkg.in/ddo/pick.v1 v1.2.2 // indirect
)
